Python Compressed Sensing algorithms
-------------------------------------

Python implementation of various Compressed Sensing algorithms originally implemented in Matlab.

## Original author
> Nicolae Cleju
> Technical University Gheorghe Asachi of Iasi, Romania
> Queen Mary, University of London
> 24.02.2012

## How to install
Run `python setup.py install`

## Works in progress
- Port to Python3: May 2016, Maxime Woringer
